import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file into a DataFrame
data = pd.read_csv('locust_results/locust_out_stats_history.csv')

# Convert the 'Timestamp' to a datetime format that Matplotlib understands
data['Timestamp'] = pd.to_datetime(data['Timestamp'], unit='s')

# Plotting the 'User Count', 'Requests/s', and 'Failures/s' over time
plt.figure(figsize=(15, 7))

plt.plot(data['Timestamp'], data['User Count'], label='User Count')
plt.plot(data['Timestamp'], data['Requests/s'], label='Requests/s')
plt.plot(data['Timestamp'], data['Failures/s'], label='Failures/s')

# Formatting the plot
plt.title('Locust Metrics Over Time')
plt.xlabel('Timestamp')
plt.ylabel('Counts')
plt.legend()
plt.grid(True)

# Rotate date labels for better readability
plt.xticks(rotation=45)

# Show the plot
plt.tight_layout()
plt.show()
