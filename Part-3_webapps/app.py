from numpy import *
from flask import Flask, jsonify, url_for

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    midpoints = []
    for i in range(N):
        midpoints.append(lower + delta_x/2 + i*delta_x)
    area = sum(abs(sin(midpoints))) * delta_x
    return area

@app.route("/")
def hello_world():
    return "<p>Hello!</p>"

@app.route('/numericalintegralservice/<float:lower>/<float:upper>')
def integrate(lower, upper):
    Ns = [10, 100, 1000, 10000, 100000, 1000000]
    results = []
    for N in Ns:
        area = numerical_integration(lower, upper, N)
        results.append(area)
    return(jsonify({"lower": lower, "upper": upper, "integral": results}))

with app.test_request_context():
    print(url_for('integrate',lower='0', upper='3.14159'))