from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def numericalIntegralService(self):
        self.client.get("/numericalintegralservice/0.0/3.14159")
