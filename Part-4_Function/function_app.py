import azure.functions as func
import logging
from numpy import *

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    midpoints = []
    for i in range(N):
        midpoints.append(lower + delta_x/2 + i*delta_x)
    area = sum(abs(sin(midpoints))) * delta_x
    return area

def integrate(lower, upper):
    Ns = [10, 100, 1000, 10000, 100000, 1000000]
    results = []
    for N in Ns:
        area = numerical_integration(lower, upper, N)
        results.append(area)
    return({"lower": lower, "upper": upper, "integral": results})
app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@app.route(route="HTTP_Trigger_4")
def HTTP_Trigger_4(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('b')
    upper = req.params.get('a')

    if upper and lower:
        try:
            lower = float(lower)
            upper = float(upper)
            integral = integrate(lower, upper)
            return func.HttpResponse(f"The integral between {lower} and {upper} is {integral['integral']}",
                                     status_code=200)
        except ValueError as e:
            return func.HttpResponse(f"Error in input parameters: {str(e)}", status_code=400)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass the upper (a) and lower (b) values in the query string to get the integral.",
             status_code=200
        )