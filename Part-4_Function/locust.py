from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def numericalIntegralService(self):
        self.client.get("/api/http_trigger_4?b=0.0&a=3.14159")
