# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

from azure.storage.blob import BlobServiceClient
from azure.storage.blob.aio import BlobClient
import os
"""
The GetInputDataFn allows us to use the blob store API and connection string to pull down the files from Azure
It will take us input the name of each file and as output a list of paris <indexline,the line string>
"""
CONNECTION_STRING = 'DefaultEndpointsProtocol=https;AccountName=mapreducefunctionazurela;AccountKey=RvXH7PYrPloxZtEhBVYfvhApyaLRg2+4xBB6vA05m6SDf1wGScnouTGSQJvWywJiXRNQj1Xgx4e/+AStkeFK8w==;EndpointSuffix=core.windows.net'
CONTAINER_NAME = 'mapreduceazurecontainer5'

def read_file(filename: str) -> list:
    blob_service_client = BlobServiceClient.from_connection_string(CONNECTION_STRING)
    container_client = blob_service_client.get_container_client(container=CONTAINER_NAME)
    blob_client = container_client.get_blob_client(blob=filename)
    stream = blob_client.download_blob().readall()

    lines = []
    for index, line in enumerate(stream.decode().splitlines()):
        lines.append((index, line))

    return lines

def main(filename: str) -> list:
    return read_file(filename)

