# Cloud Computing Lab 2

This repository contains the code and outputs used during each part of the azure lab 2
## Structure

- `Part-1_Microservice`: Contains the microservice implementation that does numerical integration.
- `Part-2_Scalesets`: Includes configurations and scripts for setting up virtual machine scale sets to improve availability.
- `Part-3_Webapps`: Holds the web application code.
- `Part-4_Function`: Contains Azure Functions for scaling microservice.
- `Part-5_MapReduce`: Implements MapReduce using Azure Durable functions.
- `all_locust_output`: Contains output files from performance tests conducted using Locust for the four cases above.

