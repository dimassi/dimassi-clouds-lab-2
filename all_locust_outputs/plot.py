import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file into a DataFrame
data1 = pd.read_csv('Part1_locust_out_stats_history.csv')
data2 = pd.read_csv('Part2_locust_out_stats_history.csv')
data3 = pd.read_csv('Part3_locust_out_stats_history.csv')
data4 = pd.read_csv('Part4_locust_out_stats_history.csv')

# Convert the 'Timestamp' to a datetime format that Matplotlib understands
data1['Timestamp'] = pd.to_datetime(data1['Timestamp'], unit='s')

# Plotting the 'User Count', 'Requests/s', and 'Failures/s' over time
plt.figure(figsize=(15, 7))
plt.plot(data1['Timestamp'], data1['Requests/s'], label='Part 1 Requests/s')
plt.plot(data1['Timestamp'], data2['Requests/s'], label='Part 2 Requests/s')
plt.plot(data1['Timestamp'], data3['Requests/s'], label='Part 3 Requests/s')
plt.plot(data1['Timestamp'], data4['Requests/s'], label='Part 4 Requests/s')


# Formatting the plot
plt.title('Locust Metrics Over Time')
plt.xlabel('Timestamp')
plt.ylabel('Counts Requests/s')
plt.legend()
plt.grid(True)

# Rotate date labels for better readability
plt.xticks(rotation=45)

# Show the plot
plt.tight_layout()
plt.show()
